package org.directoryx.service;

import org.directoryx.domain.Project;
import org.directoryx.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by svarjabe on 6/22/15.
 */

@Service("projectService")
public class ProjectServiceImpl extends GenericService<Project> implements ProjectService{

    @Autowired
    private ProjectRepository projectRepository;

    @Override
    public ProjectRepository getRepository() { return projectRepository; }

}
