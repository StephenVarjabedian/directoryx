package org.directoryx.service;

import org.directoryx.domain.Project;


public interface ProjectService extends Service<Project> {
}
