package org.directoryx.domain;

import org.springframework.data.neo4j.annotation.QueryResult;

import java.util.Map;

/**
 * Created by svarjabe on 7/14/15.
 */

@QueryResult
public class PersonWithHash {

    public Map<String, Object> person;
    public int reportCounts;

    public Map<String, Object> getPerson() {
        return person;
    }

    public int getReportCounts() {
        return reportCounts;
    }
}
