package org.directoryx.domain;

import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;
import org.springframework.security.core.GrantedAuthority;

import java.util.ArrayList;
import java.util.List;

@NodeEntity
public class Person extends Entity {

    private String emplId;
    public String name;
    public String firstName;
    public String lastName;
    public String userId;
    public String password;
    public String title;
    public String country;
    public String city;
    public String phone;
    public String email;
    public String status;
    public String photo;
    public String org;

    @Relationship(type = "FAVORITES", direction = Relationship.OUTGOING)
    private List<Person> favorites;

    @Relationship(type = "WORKS_FOR", direction = Relationship.OUTGOING)
    public Person manager;

    public Person() {}

    public String getEmplId() {
        return emplId;
    }

    public void setEmplId(String emplId) {
        this.emplId = emplId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOrg() {
        return org;
    }

    public void setOrg(String org) {
        this.org = org;
    }

    public Person getManager() {
        return manager;
    }

    public void setManager(Person manager) {
        this.manager = manager;
    }

    public void addFavorite(Person favorite) {
        if (favorites == null) {
            favorites = new ArrayList<Person>();
        }
        favorites.add(favorite);
    }

    public List<Person> getFavorites() {
        return favorites;
    }

    public Person(String emplID, String userId, String firstName, String lastName, String title) {
        this.emplId = emplID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.userId = userId;
        this.title = title;
    }

    public enum SecurityRole implements GrantedAuthority {
        ROLE_USER, ROLE_ADMIN;

        @Override
        public String getAuthority() {
            return name();
        }
    }

}
