package org.directoryx.web;

import org.apache.log4j.Logger;
import org.directoryx.domain.Person;
import org.directoryx.security.DirXUserDetails;
import org.directoryx.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;

@RestController
public class HomeController {

    static Logger log = Logger.getLogger(HomeController.class.getName());

    @Autowired
    PersonService personService;

    @RequestMapping("/")
    public ModelAndView dashboard(Authentication authentication, HttpSession session) {

        ModelAndView mav = new ModelAndView();
        Person user;

        try {
            DirXUserDetails dirXUserDetails = (DirXUserDetails) authentication.getPrincipal();
            user = dirXUserDetails.getUser();
        } catch(ClassCastException e) {
            User userDetails = (User) authentication.getPrincipal();
            user = personService.findByUserId(userDetails.getUsername());
            log.info("Recognized InMemoryAuthentication as user: user");

        }

        if(session.getAttribute("user") == null) {
            session.setAttribute("user", user);
        }

        mav.setViewName("dashboard");
        mav.addObject("user", user);

        //need to add user's favorites to mav
        //mav.addObject("favorites", personService.getFavorites(user.userId));

        return mav;
    }
}
