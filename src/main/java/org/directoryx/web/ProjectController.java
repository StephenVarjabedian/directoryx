package org.directoryx.web;

import org.apache.log4j.Logger;
import org.directoryx.Application;
import org.directoryx.domain.Project;
import org.directoryx.service.ProjectService;
import org.directoryx.service.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;


@RestController
@RequestMapping("/project")
public class ProjectController extends Controller<Project> {

    static Logger log = Logger.getLogger(Application.class.getName());


    @Autowired
    private ProjectService projectService;

    @Override
    public Service<Project> getService() {
        return projectService;
    }

    @RequestMapping("/")
    public ModelAndView getProject() {
       	ModelAndView mav = new ModelAndView();
        mav.setViewName("project/project");
        return mav;
    }
}
