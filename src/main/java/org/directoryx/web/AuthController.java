package org.directoryx.web;

import org.directoryx.service.PersonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

@Controller
@RequestMapping("/auth")
public class AuthController {

    private final static Logger log = LoggerFactory.getLogger(AuthController.class);

    @Autowired
    PersonService personService;

    @RequestMapping(value = "/login")
    public ModelAndView login(@RequestParam(value = "error", required = false) boolean error,
                              @RequestParam(value = "logout", required = false) boolean logout) {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("login");

        if(error) {
            mav.addObject("loginError", error);
        }

        if(logout) {
            mav.addObject("logout", logout);
        }

        return mav;
    }
}
