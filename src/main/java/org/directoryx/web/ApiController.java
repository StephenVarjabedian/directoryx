package org.directoryx.web;

import org.apache.log4j.Logger;
import org.directoryx.Application;
import org.directoryx.domain.Person;
import org.directoryx.service.PersonService;
import org.directoryx.service.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
@RequestMapping("/api/v1/")
public class ApiController extends Controller<Person> {
    static Logger log = Logger.getLogger(Application.class.getName());

    @Autowired
    private PersonService personService;

    @Override
    public Service<Person> getService() {
        return personService;
    }

    @RequestMapping("people/search/{term}")
    public ArrayList<Person> search(@PathVariable(value = "term") String term) {
        return personService.search(term);
    }

    /* Used in filtering AJAX call to get all employees with firstName term */
    @RequestMapping("people/employees/search/{term}")
    public ArrayList<Person> searchEmployees(@PathVariable(value = "term") String term) {
        ArrayList<Person> people = personService.findEmployeeByFirstName(term+".*");
        return people;
    }

    /* Used in filtering AJAX call to get all contractors with firstName term */
    @RequestMapping("people/contractors/search/{term}")
    public ArrayList<Person> searchContractors(@PathVariable(value = "term") String term) {
        ArrayList<Person> contractors = personService.findContractorByFirstName(term + ".*");
        return contractors;
    }

    /* Used to test if the userIds are being indexed and will prevent creation of duplicate entries */
    @RequestMapping("test/prevent/dup/{term}")
    public Person createDuplicate(@PathVariable(value = "term") String term) {
        Person person = personService.findByUserId(term);
        Person dup = new Person();
        dup.setUserId(term);
        dup.setEmail(term+"@cisco.com");
        personService.savePerson(dup);
        return dup;
    }





}
