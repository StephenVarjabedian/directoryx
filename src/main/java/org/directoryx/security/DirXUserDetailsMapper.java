package org.directoryx.security;

import org.directoryx.repository.PersonRepository;
import org.directoryx.security.DirXUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.ldap.userdetails.LdapUserDetailsMapper;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
public class DirXUserDetailsMapper extends LdapUserDetailsMapper {

    @Autowired
    PersonRepository personRepository;

    @Override
    public DirXUserDetails mapUserFromContext(DirContextOperations dirContextOperations, String userId, Collection<? extends GrantedAuthority> collection) {
        return new DirXUserDetails(personRepository.findByUserId(userId));
    }
}
