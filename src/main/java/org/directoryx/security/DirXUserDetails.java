package org.directoryx.security;

import org.directoryx.domain.Person;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.ldap.userdetails.LdapUserDetails;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;


public class DirXUserDetails implements LdapUserDetails {

    private final Person user;

    public DirXUserDetails(Person user) {
        this.user = user;
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {

        return new ArrayList<>();

        //uncomment when authorization is implemented, is used to get Person's roles to determine what they have access to.
//        Person.SecurityRole[] roles = user.getRole();
//        if (roles == null) {
//            return Collections.emptyList();
//        }
//        return Arrays.<GrantedAuthority>asList(roles);
    }

    @Override
    public String getPassword() {
        return null;
    }

    @Override
    public String getUsername() {
        return user.getUserId();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public Person getUser() {
        return user;
    }

    @Override
    public String getDn() {
        return null;
    }
}
