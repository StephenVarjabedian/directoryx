package org.directoryx;

import org.neo4j.ogm.session.Session;
import org.neo4j.ogm.session.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.core.env.Environment;
import org.springframework.data.neo4j.config.Neo4jConfiguration;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;
import org.springframework.data.neo4j.server.Neo4jServer;
import org.springframework.data.neo4j.server.RemoteServer;
import org.springframework.data.neo4j.template.Neo4jOperations;
import org.springframework.data.neo4j.template.Neo4jTemplate;
import org.springframework.data.repository.query.QueryLookupStrategy;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.Resource;

@Configuration
@EnableNeo4jRepositories(basePackages = "org.directoryx.repository", queryLookupStrategy = QueryLookupStrategy.Key.CREATE_IF_NOT_FOUND)
@EnableTransactionManagement
public class Neo4jConfig extends Neo4jConfiguration {

    private final Logger log = LoggerFactory.getLogger(Neo4jConfig.class);

    @Resource
    public Environment env;

    @Override
    @Bean
    public Neo4jServer neo4jServer() {
        log.info("Initialising Neo4j server connection...");

        // username and password need to be available as System properties for Neo4j authentication.
        String user = env.getProperty("neo4j.user");
        System.setProperty("username", user);

        String pass = env.getProperty("neo4j.pass");
        System.setProperty("password", pass);

        log.info("connecting as neo4j.user=" + user);
        return new RemoteServer("http://localhost:7474");
    }

    @Override
    @Bean
    public SessionFactory getSessionFactory() {
        log.info("Initialising Session Factory");
        return new SessionFactory("org.directoryx.domain");
    }



}