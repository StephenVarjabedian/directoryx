Prerequisites:
====================

1. Install Apache Maven (https://maven.apache.org/)
2. Install Neo4j 2.2.2+  (http://neo4j.com/)
3. Install RabbitMQ (https://www.rabbitmq.com)
    * Note: We recommend installing the Rabbit management module too, so you can review the queue and send test messages. See https://www.rabbitmq.com/management.html

Quick Start:
====================

Start up the database, the message queue, configure the web app and run it. Done! :)

1. Start Neo4j Server
    * ```$NEO4J_HOME/bin/neo4j start```
    * Browse http://localhost:7474 and set a new password
    * [Load some QuickStart Data](https://rigorous.atlassian.net/wiki/display/DX/Quickstart+Organization+Data)
2. Start the RabbitMQ Server
    * ```$RABBITMQ_HOME/sbin/rabbitmq-server```
    * FYI - the app will run without RabbitMQ, but will throw many warning and error messages.
3. Run the application with Spring Boot:
    * create an *application.properties* file in the root project folder
    * set the neo4j password property in the local application.properties file
    * Review the [default application.properties](https://bitbucket.org/rigorous/directoryx/src/d21edcabf9c9f73062649862944c51bcf5bfbbf9/src/main/resources/application.properties?at=master) to see all properties that may need to be set in your local secure *application.properties* file 
    * **Run** the main class from your IDE, or via Maven: ```mvn spring-boot:run```
4. Browse http://localhost:8080
    * Default User/pass for in-memory authentication is simply "user" and "foo".

Optional
==============

We created a custom dataset generator and import script called "People Maker (https://github.com/ianrtracey/people-maker) to easily generate 100,000 records for testing.

Use the People Maker documentation to generate and import the dataset

Notes:

* Make sure the database specified by the Neo4j Batch Importer is the same as specified in conf/neo4j-server.properties
* Ensure Neo4j is running when you run batch import and restart the server after the import
* You may receive warnings after import that may be ignored
* Check the file paths of your batch.properties inside of Batch Import
* Make sure batch_import.csv.delim=, is uncommented in batch.properties if using CSV
* Your security/SecurityConfig.java will need to change based on your login credentials

Introduction
====================

A project to build a next generation directory **web application** that is mobile-ready, responsive, and 
focused on the relationships of a typical **_business network_**. 

* People
* Projects
* Teams
* Places

We plan to use a graph database (Neo4j) to store the objects and relationships, 
so the system can scale to literally billions of nodes and connections. 

Key features
-----------

* Search 
    * Auto-completion for userid/username
	* Soundex matching
	* Project name and description search
* Dashboard that shows the currently logged in users connections.
    * People: Current, Peers, Reports (if present), Manager
	* Places: Current, Favorites
	* Teams: active teams
	* Projects: active projects, link to the archives.
* Web Service REST API
	* Views of the data objects
	* Updating and adding new network objects.
* Detail Views
	* Details of a person
	* Details for a group of people
	* Details of a Project, Place, Team
* Browsing
	* Visualization (ala D3JS.org) of a group / team
	* Projects rolled up into larger programs/initiatives
	* Matrix reporting structures